import json
from numpy import sign
from solcx import compile_standard, install_solc
from web3 import Web3
from dotenv import load_dotenv
import os

load_dotenv()

solc_version = "0.8.0"
with open("./SimpeStorage.sol", "r") as file:
    simple_storage_file = file.read()

# Compile our sol code
install_solc(solc_version)
compiled_sol = compile_standard(
    {
        "language": "Solidity",
        "sources": {"SimpleStorage.sol": {"content": simple_storage_file}},
        "settings": {
            "outputSelection": {
                "*": {"*": ["abi", "metadata", "evm.bytecode", "evm.sourceMap"]}
            }
        },
    },
    solc_version=solc_version,
)

with open("./compiled_code.json", "w") as file:
    json.dump(compiled_sol, file)


# Get bytecode
bytecode = compiled_sol["contracts"]["SimpleStorage.sol"]["SimpleStorage"]["evm"][
    "bytecode"
]["object"]
# Get abi
abi = compiled_sol["contracts"]["SimpleStorage.sol"]["SimpleStorage"]["abi"]

print("Connecting to block...")
w3 = Web3(
    Web3.HTTPProvider("https://ropsten.infura.io/v3/47787db669a54fd4a016f66198ff45bd")
)
my_address = "0xAD8A546B5FF6209F78d4cb0Ea3D844600FFefF34"
private_key = os.getenv("PRIVATE_KEY")

# Create the contract in python
SimpleStorage = w3.eth.contract(abi=abi, bytecode=bytecode)
nonce = w3.eth.getTransactionCount(my_address)
# 1. Build TX
# 2. Sign TX
# 3. Send TX
transaction = SimpleStorage.constructor().buildTransaction(
    {
        "gasPrice": w3.eth.gas_price,
        "chainId": w3.eth.chain_id,
        "from": my_address,
        "nonce": nonce,
    }
)
signed_tx = w3.eth.account.sign_transaction(transaction, private_key=private_key)
print("Deploying contract...")
tx_hash = w3.eth.send_raw_transaction(signed_tx.rawTransaction)
tx_receipt = w3.eth.wait_for_transaction_receipt(tx_hash)
print("Contract deployed!")

# To interact with a contract, you will need:
# 1. Contract address
# 2. Contract ABI
simple_storage = w3.eth.contract(address=tx_receipt.contractAddress, abi=abi)


# Call -> dry run (no state change)
# Transact -> does change the state
print("People count: ", simple_storage.functions.peopleCount().call())

signup_tx = simple_storage.functions.signup("farhan", 34).buildTransaction(
    {
        "gasPrice": w3.eth.gas_price,
        "chainId": w3.eth.chain_id,
        "from": my_address,
        "nonce": nonce + 1,
    }
)
signed_tx = w3.eth.account.sign_transaction(signup_tx, private_key=private_key)
print("Someone is signing up...")
tx_hash = w3.eth.send_raw_transaction(signed_tx.rawTransaction)
tx_recipt = w3.eth.wait_for_transaction_receipt(tx_hash)
print("People count: ", simple_storage.functions.peopleCount().call())
